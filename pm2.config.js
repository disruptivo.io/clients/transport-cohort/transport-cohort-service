module.exports = {
  apps : [
    {
      name   : 'transport-cohort-service',
      script : 'yarn start:prod',
      cwd: '/app/transport-cohort-service',
      env: {
        NODE_ENV: 'development',
      },
      env_production: {
        NODE_ENV: 'production',
        DATABASE_URL: 'file:/app/transport-cohort-db.db'
      },
    }
  ],
  deploy: {
    production: {
      user: 'deploy',
      host: ['localhost'],
      ref: 'origin/main',
      repo: 'https://gitlab.com/disruptivo.io/clients/transport-cohort/transport-cohort-service.git',
      path: '/app/transport-cohort-service',
      'post-setup': 'git submodule update --init --recursive', // run after cloning the repo
      'pre-deploy': 'git pull origin main && git submodule update --recursive && yarn install && yarn build',
    },
  },
}