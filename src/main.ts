import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule } from '@nestjs/swagger';
const openapi = require('../transport-cohort-db/openapi.json');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  
  app.enableCors();
  SwaggerModule.setup('/', app, openapi);

  await app.listen(3000);
}
bootstrap();
