import { Module, MiddlewareConsumer, NestModule } from '@nestjs/common';
import { PrismaService } from './prisma.service';
import { ZenStackMiddleware } from '@zenstackhq/server/express';
import { withPresets } from '@zenstackhq/runtime';

@Module({
  imports: [],
  providers: [PrismaService],
})
export class AppModule implements NestModule {

  constructor(private prismaService: PrismaService) {}

  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply((ZenStackMiddleware({ getPrisma: () => withPresets(this.prismaService) })))
      .forRoutes('/api/v1');
  }
}
